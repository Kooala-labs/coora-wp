<?php
/**
 * Template Name: Listing - Collections
 * Chipmunk: Page Collections
 *
 * @package WordPress
 * @subpackage Chipmunk
 */

get_header(); ?>

	<?php get_template_part( 'templates/sections/collections' ); ?>

<?php get_footer(); ?>
