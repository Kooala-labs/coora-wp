<?php
/**
 * Chipmunk: Archive
 *
 * @package WordPress
 * @subpackage Chipmunk
 */

get_header(); ?>

	<?php get_template_part( 'templates/sections/author' ); ?>
	<?php get_template_part( 'templates/sections/resources' ); ?>

<?php get_footer(); ?>
