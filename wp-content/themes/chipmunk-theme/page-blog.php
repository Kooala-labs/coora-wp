<?php
/**
 * Template Name: Listing - Blog
 * Chipmunk: Page Blog
 *
 * @package WordPress
 * @subpackage Chipmunk
 */

get_header(); ?>

	<?php get_template_part( 'templates/sections/posts' ); ?>

<?php get_footer(); ?>
