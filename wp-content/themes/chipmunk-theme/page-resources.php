<?php
/**
 * Template Name: Listing - Resources
 * Chipmunk: Page Resources
 *
 * @package WordPress
 * @subpackage Chipmunk
 */

get_header(); ?>

	<?php get_template_part( 'templates/sections/resources' ); ?>

<?php get_footer(); ?>
