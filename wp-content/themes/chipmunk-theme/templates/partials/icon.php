<svg class="icon icon--<?php echo $icon; ?> icon--<?php echo isset( $size ) ? $size : 'md'; ?>">
	<use xlink:href="<?php echo get_template_directory_uri(); ?>/static/dist/images/icon-sprite.svg#<?php echo $icon; ?>"></use>
</svg>
